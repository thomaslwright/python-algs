#!/usr/bin/env python2.6
import sys

myargs = {}

# argv[0] is the command so skip it
print "args in:",
for arg in sys.argv[1:]:
    print arg,

print ", args out:",
for arg in sys.argv[1:]:
    if not arg in myargs:
        print arg,
        myargs[arg] = 1

#!/usr/bin/env python3 

import glob
import os
import sys
from optparse import OptionParser
[...]
parser = OptionParser()
parser.add_option("-D", dest="logdir",
                  help="log file directory", metavar="DIR")
(options, args) = parser.parse_args()

if not options.logdir:
   parser.error("-D logdir must be specified")

logs = glob.glob(options.logdir+"/*.log")
for file in logs:
  if os.path.isfile(file):
   stat = os.stat(file)
   print(file, stat.st_atime)

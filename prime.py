#!/usr/bin/python
import math
def prime(n):
    if n < 2: return False
    s = int(math.sqrt(n))
    # print "n", n, "s", s
    for i in range(2, s + 1):
        # print "i", i, "n%i", n%i
        if not (n % i): return False
    return True
print "prime(1) is", prime(1)
print "prime(2) is", prime(2)
print "prime(7) is", prime(7)
print "prime(9) is", prime(9)
print "prime(15) is", prime(15)
print "prime(16) is", prime(16)
print "prime(17) is", prime(17)

#!/usr/bin/python

""" 
A python program to find distance between n1  
and n2 in binary tree 
"""
# binary tree node 
class Node: 
    # Constructor to create new node 
    def __init__(self, key): 
        self.val = key 
        self.left = self.right = None
    
# This function returns pointer to LCA of  
# two given values n1 and n2. 
def LCA(root, n1, n2): 
    # Base case 
    if root is None: 
        return None  
    # If either n1 or n2 matches with root's 
    # key, report the presence by returning 
    # root  
    if root.val == n1 or root.val == n2: 
        return root 
    # Look for keys in left and right subtrees 
    left = LCA(root.left, n1, n2) 
    right = LCA(root.right, n1, n2) 
    if left is not None and right is not None: 
        return root 
    # Otherwise check if left subtree or  
    # right subtree is LCA 
    if left: 
        return left 
    else: 
        return right 

# function to find distance of any node 
# from root 
def findLevel(root, data, d, level): 
    # Base case when tree is empty 
    if root is None: 
        return
    # Node is found then append level 
    # value to list and return 
    if root.val == data: 
        d.append(level) 
        return
    findLevel(root.left, data, d, level + 1) 
    findLevel(root.right, data, d, level + 1) 
  
# function to find distance between two 
# nodes in a binary tree 
def findDistance(root, n1, n2): 
    lca = LCA(root, n1, n2) 
    # to store distance of n1 from lca 
    d1 = []  
    # to store distance of n2 from lca 
    d2 = []  
    # if lca exist 
    if lca: 
        # distance of n1 from lca 
        findLevel(lca, n1, d1, 0)  
        # distance of n2 from lca 
        findLevel(lca, n2, d2, 0)  
        return d1[0] + d2[0] 
    else: 
        return -1

# A utility function to insert a new node with the given key 
def insert(root,node): 
    if root is None: 
        root = node 
    else: 
        if root.val < node.val: 
            if root.right is None: 
                root.right = node 
            else: 
                insert(root.right, node) 
        else: 
            if root.left is None: 
                root.left = node 
            else: 
                insert(root.left, node) 

def bstdistance(l, n1, n2):
    r = Node(l[0])
    for n in l[1:]:
        insert(r, Node(n))
    return findDistance(r, n1, n2)

# Driver program to test above function 
#      5
#    3   6
#  1  4
#   2
l = [5, 6, 3, 1, 2, 4]
n1 = 2
n2 = 4
d = bstdistance(l, n1, n2)
print "Dist({n1}, {n2}) = {d}".format(n1=n1, n2=n2, d=d)

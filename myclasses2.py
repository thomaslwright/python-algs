#!/usr/bin/env python

class mytest(object):
    def __init__(self, prompta, promptb, promptc):
        self.prompta = prompta
        self.promptb = promptb
        self.promptc = promptc
        
        self.sendexpect = [
            ['line1', 'prompt1'],
            ['line2', 'prompt2'],
            ['line3', self.prompta],
            ['line4', self.promptb],
            ['line5', self.promptc],
            ['line6', 'prompt3'],
            ]
        print "sendexpect = %s," % self.sendexpect
        
        def send(self, line):
            print "line = %s," % line,
            
        def expect(self, prompt):
            print "prompt = %s" % prompt
                
        def dosendexpect(self):
            for line in self.sendexpect:
                self.send(line[0])
                self.expect(line[1])
                
def main():
    prompta = 'promptx'
    promptb = 'prompty'
    promptc = 'promptz'
    thetest = mytest(prompta, promptb, promptc)
    thetest.dosendexpect()
            
if __name__ == '__main__':
    main()

#!/usr/bin/env python3 


import time

o1 = open("log1.log", 'r')
o2 = open("log2.log", 'r')
print("o1.fileno()=",o1.fileno()) # 3
print("o2.fileno()=",o2.fileno()) # 4
o1.seek(0, 2) # end
o2.seek(0, 2) # end

while True:
    for line in o1:
        print(line, end='')
    
    while True:
        l2 = o2.readline()
        if l2 == '': break
        print(l2, end="")

    time.sleep(1)

#!/usr/bin/env python3 

import json

s1 = '{"content":{"key1":"value1","key2":{"key2.1":"value2.1","key2.2":"value2.1"},"key3":"Value3"}, "at": "Fri Jul 4 13:27:11 PDT 2014", "note":"note content"}'
s2 = '{"content":{"key1":"value1","key2":{"key2.1":"value2.1","key2.2":"value2.1"},"key3":"Value3"}, "at": "Fri Jul 4 13:27:11 PDT 2014"}'
s3 = 'ERROR: could not render object: no such field!'

print("s1=", s1)
print("str(s1)=", str(s1))
print("repr(s1)=", repr(s1))
print('''s1.replace('\"', '\\"')=''', s1.replace('\"', '\\"'))

# Deserialize s (a str instance containing a JSON document) to a Python object
z = json.loads(s1)
print("z=json.loads(s)=", z)
print("z[\"content\"]=", z["content"])
print("z[\"at\"]=", z["at"])
print("z[\"note\"]=", z["note"])

# Serialize obj to a JSON formatted str
x = json.dumps(z)
print("x=json.dumps(z)=", x)

# pretty
print("\n", json.dumps(z, sort_keys=True, indent=4, separators=(',', ':')))

z2 = json.loads(s2)
if "note" in z2: print("z[\"note\"]=", z["note"])
else: print("ERROR: z2 mising note")

try:
    z3 = json.loads(s3)
except ValueError:
    print("ERROR Invalid JSON stanza", s3)

#!/usr/bin/env python3 
# -D dir (req'd)
# -T maxwait (opt)
# -B (opt)
import sys
from optparse import OptionParser
[...]
parser = OptionParser()
parser.add_option("-D", dest="logdir",
                  help="log file directory", metavar="DIR")
parser.add_option("-T", dest="maxwait",
                  help="max wait in ms", metavar="MAXWAIT", default=1000)
parser.add_option("-B", dest="beginning", action="store_true",
                  help="read from beginning", default=False)

(options, args) = parser.parse_args()

if not options.logdir:
   parser.error("-D logdir must be specified")
if args:
   print ("extra args", args, file=sys.stderr)
   parser.error("extra args not recognised")
print (options, args)
#print (logdir, maxwait, beginning)

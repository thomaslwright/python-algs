#!/usr/bin/env python3 

# see man poll
# see /Developer/SDKs/MacOSX10.7.sdk/usr/include/sys/poll.h

import os, select, time

o1 = open("log1.log", 'r')
o2 = open("log2.log", 'r')
print("o1.fileno()=",o1.fileno()) # 3
print("o2.fileno()=",o2.fileno()) # 4
o1.seek(0, 2) # end
o2.seek(0, 2) # end

p = select.poll()
p.register(o1.fileno(), select.POLLIN | select.POLLPRI | select.POLLERR)
p.register(o2.fileno(), select.POLLIN | select.POLLPRI | select.POLLERR)

while True:
    ret = p.poll(1000)
    print("ret=", ret)
# ret= [(3, 1), (4, 1)]
#    print("ret[0]=", ret[0], end='')
#    print("ret[0][0]=", ret[0][0], end='')
#    print("ret[0][1]=", ret[0][1], end='')
#    print("ret[1]=", ret[1])

#    print("o1.readable()=", o1.readable())
    for line in o1:
        print(line, end='')
#    print("o1.readable()=", o1.readable())
#    l1=o1.readline()
#    print("l1='" + l1 + "'")
    
    while True:
        l2 = o2.readline()
        if l2 == '': break
        print(l2, end="")

    time.sleep(1)
    

#!/usr/bin/env python3 

# Fri Jul 4 13:27:11 PDT 2014
# %a  %b %d %H %M %S %Z  %Y

import time
#from datetime import datetime
import datetime

format = "%a %b %d %H:%M:%S %Z %Y"
timestamp = "Fri Jul 4 13:27:11 PDT 2014"
logtime = datetime.datetime.strptime(timestamp, format)
print ("logtime=", logtime)
print ("logtime.ctime()=",logtime.ctime())
print ("logtime.timestamp()=", logtime.timestamp())
print ("time.time()=",time.time())

now = datetime.datetime.today()
print("now=", now.strftime(format))

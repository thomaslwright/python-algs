#!/usr/bin/env python2.6

grades = {}
fh = open("grades")

for line in fh:
    line = line.rstrip('\n')
    (student, grade) = line.split()
    #print "student %s grade %s" % (student, grade) 
    if student in grades: grades[student] += grade + " "
    else: grades[student] = grade + " "
                
for student in sorted(grades):
    scores = 0
    total = 0
    gradesa = grades[student].split()
    for grade in gradesa:
        total += float(grade)
        scores += 1
    average = total / scores
    print "%s: %s \tAverage %s" % (student, grades[student], average)

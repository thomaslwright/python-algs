#!/usr/bin/python
# See ~/public_html/jobs/er18/amazon.txt
def cells(d, c):
    o = c[:]
    n = c[:]
    size = len(c)
    last = size - 1
    for x in range(d):
        for s in range(size):
            if s == 0:
                n[s] = 0 if o[s+1] == 0 else 1 
            elif s == last:
                n[s] = 0 if o[s-1] == 0 else 1 
            else:
                n[s] = 0 if o[s-1] == o[s+1] else 1 
        #print o
        #print n
        o = n[:]
    return n

cs  = ( (1, [1, 0, 0, 0, 0, 1, 0, 0]), (2, [1, 1, 1, 0, 1, 1, 1, 1]) )
for c in cs:
    x = cells(c[0], c[1])
    print "cells({d}, {cc}) is {x}".format(d=c[0], cc=c[1], x=x) 

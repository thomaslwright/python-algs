#!/usr/bin/env sh
set -x
./getopts.py -h
./getopts.py
./getopts.py -D
./getopts.py -D mydir
./getopts.py -D mydir -B
./getopts.py -D mydir -T
./getopts.py -D mydir -T 100
./getopts.py -D mydir -T 100 -B
./getopts.py -D mydir aaa

#!/usr/bin/python
# See ~/public_html/jobs/er18/amazon.txt
def substrs(ss, num):
    l = len(ss)
    out = []
    for i in range(l-num):
        w = ss[i:i+num]
        #print w
        s = set()
        for c in w:
            if c in s:
                break
            else: 
                s.add(c)
        else:
            if w not in out:
                out.append(w)
    return out
for l in (("awaglknagawunagwkwagl", 4),):
    x = substrs(l[0], l[1])
    print "substrs({l}) is {x}".format(l=l, x=x) 

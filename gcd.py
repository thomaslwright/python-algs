#!/usr/bin/python
# See ~/public_html/jobs/er18/amazon.txt
def gcd(l):
    m = min(l)
    for i in range(m, 0, -1):
        for n in l:
            # print n, i, n % 1
            if n % i:
                break
        else: 
            return i
for l in ((2, 3, 4, 5, 6), (2, 4, 6, 8, 10)):
    x = gcd(l)
    print "gcd({l}) is {x}".format(l=l, x=x) 

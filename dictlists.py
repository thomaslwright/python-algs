#!/usr/bin/env python3 

# key and 1 or more lists
buf = {}

def rcdappend(key, line, index, valid):
    print(rcdappend)
    print(buf)
    if key in buf: 
        buf[key].append(line)
        buf[key].append(index)
        buf[key].append(valid)
    else: buf[key] = [line, index, valid]
    
def rcdpop(key):
    print(rcdpop)
    print(buf)
    if key in buf: 
        print(buf[key])
        print(buf[key].pop(0))
        print(buf[key].pop(0))
        print(buf[key].pop(0))
    else: print("key not found")

rcdappend(123, "line1", 0, True)
rcdappend(123, "line2", 1, False)
rcdpop(123)
rcdpop(123)
print(buf)
print(len(buf[123]))
if len(buf[123]) == 0: del buf[123]
print(buf)

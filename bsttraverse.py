#!/usr/bin/python
# See ~/public_html/jobs/er18/amazon.txt
import copy
def bsttraverse(v, n1, n2):
    # https://www.tutorialspoint.com/python/python_binary_search_tree.htm ?
    # ~/public_html/src/algs/bst*
    # create bst from v
    # traverse bst from n1 to n2, return distance bewteen nodes
    #        5
    #     3    6
    #   1  4
    #    2
    left = 'left'
    right = 'right'
    bst = {v[0]: {left: None, right: None}}
    for x in v[1:]:
        z = bst
        while z:
            k = z.keys()[0]
            print x, k, z
            if x < k:
                print left, z[k]
                z = z[k][left]
            else:
                print right, z[k]
                z = z[k][right]
        print 'z', z
        z = {x: {left: None, right: None}}  # XXX does not seem to update bst!
        print 'z', z, 'bst', bst
    print 'bst', bst

v = [5, 6, 3, 1, 2, 4]
n1=2 
n2=4
x = bsttraverse(v=v, n1=n1, n2=n2)
print "bsttraverse({v}, {n1}, {n2}) is {x}".format(
    v=[5, 6, 3, 1, 2, 4], n1=2, n2=4, x=x)

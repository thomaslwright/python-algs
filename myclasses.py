#!/usr/bin/env python

class c1(object):
    def __init__(self):
        print "c1 init"
    def h1(self):
        print "c1 hi"
            
class c2(c1):
    def __init__(self):
        print "c2 init 1"
        super(c2, self).__init__()
        print "c2 init 2"
    def h2(self):
        print "c2 hi"
        
class c3(c1):
    def __init__(self):
        print "c3 init 1"
        c1.__init__(self)
        print "c3 init 2"
    def h3(self):
        print "c3 hi"
                                    
myc1 = c1()
myc2 = c2()
myc3 = c3()
                                    
myc1.h1()
myc2.h1()
myc2.h2()
myc3.h1()
myc3.h3()

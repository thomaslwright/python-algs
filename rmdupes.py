#!/usr/bin/env python2.6
import sys

myargs = {}

# argv[0] is the command so skip it
print "args in:",
for arg in sys.argv[1:]:
    print arg,
    if arg in myargs: myargs[arg] += 1
    else: myargs[arg] = 1

print ", args out:",
for arg in sys.argv[1:]:
    if myargs[arg] == 1: print arg,


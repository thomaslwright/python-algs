#!/usr/bin/env python3 

i1 = open("/usr/share/dict/connectives", 'r')
i2 = open("/usr/share/dict/propernames", 'r')
i3 = open("/usr/share/dict/words", 'r')
o1 = open("log1.log", 'w')
o2 = open("log2.log", 'w')
o3 = open("log3.log", 'w')

for i in range(100):
    l1 = i1.readline()
    l2 = i2.readline()
    l3 = i3.readline()
    o1.write(l1)
    o2.write(l2)
    o3.write(l3)

i1.close()
i2.close()
i3.close()
o1.close()
o2.close()
o3.close()

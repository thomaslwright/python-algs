#!/usr/bin/python

# A utility class that represents an individual node in a BST 
class Node: 
    def __init__(self,key): 
        self.left = None
        self.right = None
        self.val = key 
  
# A utility function to insert a new node with the given key 
def insert(root,node): 
    if root is None: 
        root = node 
    else: 
        if root.val < node.val: 
            if root.right is None: 
                root.right = node 
            else: 
                insert(root.right, node) 
        else: 
            if root.left is None: 
                root.left = node 
            else: 
                insert(root.left, node) 
  
# A utility function to do inorder tree traversal 
def inorder(root): 
    if root: 
        inorder(root.left) 
        print(root.val) 
        inorder(root.right) 

# A utility function to do breadth tree traversal 
def btraverse(root, first):
    if not root: return
    first.append(root)  # enqueue
    while(root):
        if not first: return None
        root = first.pop(0)  # dequeue
        if root: 
            print root.val
            if root.left: first.append(root.left)  # enqueue
            if root.right: first.append(root.right)  # enqueue
    return

# Driver program to test the above functions 
# Let us create the following BST 
#      50 
#    /      \ 
#   30     70 
#   / \    / \ 
#  20 40  60 80 
vals = (50, 30, 20, 40, 70, 60, 80)
r = Node(vals[0])
for val in vals[1:]: 
    insert(r,Node(val)) 

# Print inorder traversal of the BST 
print "inorder"
inorder(r) 

# see algs/bstq.c enqueue, dequeue and btraverse re breadth traversal
# see deque.py re collections deque instead of list as in queue.py
first = []  # list
print "breadth"
btraverse(r, first)

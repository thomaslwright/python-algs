#!/usr/bin/env python3 

# handle kill (sig: term 15, int 2)

import signal, os, time

def handler(signum, frame):
    print ('Signal handler called with signal', signum)

signal.signal(signal.SIGTERM, handler)
signal.signal(signal.SIGINT, handler)

time.sleep(60)
print ("woke up")
